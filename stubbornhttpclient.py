#!/usr/bin/env python3

import http.client
import itertools
import sys
import socket
import time

class StubbornHTTPClient:
	class AddressRotator:
		def __init__(self, addrs, fail_delay=2):
			self._addrs = itertools.cycle([[a,0] for a in addrs])
			self._addr = next(self._addrs)

			self._fail_delay = fail_delay

		def get(self):
			return self._addr[0]

		def rotate(self):
			self._addr[1] = time.time()
			self._addr = next(self._addrs)
			self._failDelay()

		def _failDelay(self):
			delay = time.time() - self._addr[1]
			if delay < self._fail_delay:
				sleep(self._fail_delay - delay)

	def __init__(self, addrs, host):
		self._addr = self.AddressRotator(addrs)
		self._host = host

		self._conn = None

	def _connect(self):
		self._conn = http.client.HTTPConnection(self._addr.get(), timeout=2)

	def _read(self, url):
		self._conn.request(
			'GET', url,
			headers = {'Host': self._host}
		)
		return self._conn.getresponse().read().decode()

	def read(self, url):
		while True:
			try:
				if not self._conn:
					self._connect()
				data = self._read(url)
				return data
			except (http.client.HTTPException, socket.timeout) as e:
				self._addr.rotate()
				self._conn = None
				continue

