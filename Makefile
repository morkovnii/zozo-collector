all:

clean:

install:
	install -d $(DESTDIR)/usr/lib/zozocollector/
	install -d $(DESTDIR)/usr/bin/
	install -d $(DESTDIR)/lib/systemd/system/
	install -d $(DESTDIR)/etc/zozocollector/
	install -m 644 playtime.py stubbornhttpclient.py $(DESTDIR)/usr/lib/zozocollector/
	install zozocollector.py $(DESTDIR)/usr/lib/zozocollector/
	install zozocollector $(DESTDIR)/usr/bin/
	install -m 644 zozocollector.service $(DESTDIR)/lib/systemd/system/
	install -m 644 conf/graphite.conf $(DESTDIR)/etc/zozocollector/
	install -m 644 conf/zozo.yml $(DESTDIR)/etc/zozocollector/

.PHONY: install clean all
