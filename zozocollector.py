#!/usr/bin/env python3

import time
import sys
import yaml
import itertools

from playtime import PlayTime
from stubbornhttpclient import StubbornHTTPClient

#CONF_PATH = 'conf/zozo.yml'
CONF_PATH = '/etc/zozocollector/zozo.yml'

conf = yaml.safe_load(open(CONF_PATH))

zozoreader = StubbornHTTPClient(
	[a['host'] for a in conf['zozo_ips']],
	'zo-zo.org',
)
playtime = PlayTime(
	[PlayTime.Server(s['name'], int(s['id'])) for s in conf['game_servers']]
)

iter_time = time.time()
while True:
	jobs = playtime.jobs()
	for url, parser in jobs:
		for mt_name, mt_val in parser(zozoreader.read(url)):
			print("zozoplaytime.%s %s %d" % (mt_name, mt_val, time.time()))

	sys.stdout.flush()

	iter_time += 15
	skipped_iterations = 0
	while time.time() > iter_time:
		iter_time += 15
		skipped_iterations += 1
	if skipped_iterations > 0:
		print(
			"Warning: a poll iteration took too much time. Skipping %d iterations"
				% skipped_iterations,
			file=sys.stderr
		)
	time.sleep(iter_time - time.time())
	
