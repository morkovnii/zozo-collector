import re
import functools
import copy
import collections

MAX_PLAYERS=22

class PlayTime():
	Server = collections.namedtuple('Server', ['name','id'])

	_url = '/hlstatsx/status.php?server_id=%d&game=l4d2'

	def __init__(self, servers):
		self._jobs = list((self._url % id, self._parser(name)) for name, id in servers)

	def jobs(self):
		return copy.copy(self._jobs)

	def _parser(self, serv_name):
		numberer = PlayerNumberer()
		return lambda page: self._parse(page, serv_name, numberer)

	@classmethod
	def _parse(cls, page, serv_name, numberer):
		playerid_playtime = PlaytimeReader.readPlaytime(page)
		playerid_playtime = cls._uniquePlayerids(playerid_playtime)
		playerid_playtime = sorted(playerid_playtime, key=lambda x: x[1], reverse=True)[:MAX_PLAYERS]
		playerid_number = numberer.number([playerid for playerid, _ in playerid_playtime])
		return [('%s.player%d' % (serv_name, playerid_number[id]), '%d' % time) for id, time in playerid_playtime]

	@staticmethod
	def _uniquePlayerids(playerid_playtime):
		pp = dict()
		for pid, time in playerid_playtime:
			if pid in pp:
				pp[pid] = min(pp[pid], time)
			else:
				pp[pid] = time
		return list(pp.items())


class PlaytimeReader:
	_playtime_re = re.compile(
		r'<tr[^>]+><td[^>]+><a[^>]+player=(?P<player_id>\d+)[^>]+><img[^>]+>&nbsp;'\
		r'<span[^>]+>[^<]*</span></a></td><td[^>]+>(?P<hours>\d+):(?P<minutes>\d+):(?P<seconds>\d+)</td></tr>'
	)

	@staticmethod
	def _readMatch(m):
		id = m.group('player_id')
		hms_online = [int(m.group(group_name)) for group_name in ['hours', 'minutes', 'seconds']]
		seconds_online = functools.reduce(lambda total, x: total*60+x, hms_online, 0)
		return (id, seconds_online)

	@classmethod
	def readPlaytime(self, page):
		return [self._readMatch(m) for m in self._playtime_re.finditer(page)]


class PlayerNumberer:
	def __init__(self):
		self._oldnums = {}
		self._available_numbers = list(reversed(range(1,MAX_PLAYERS+1)))

	def number(self, player_ids):
		nums = {}
		free_nums = copy.copy(self._available_numbers)
		unmatched_ids = []
		for id in player_ids:
			num = self._oldnums.get(id)
			if not num:
				unmatched_ids.append(id)
				continue
			free_nums.remove(num)
			nums[id] = num
		for id in unmatched_ids:
			nums[id] = free_nums.pop()
		self._oldnums = nums
		return nums

